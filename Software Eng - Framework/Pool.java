package com.kilobolt.framework;

import java.util.ArrayList;
import java.util.List;

public class Pool<T> {

    /**
     * This Interface is used to generate objects of type T
     * @param <T> The type of object which will be created.
     */
    public interface PoolObjectFactory<T> {
        /**
         * Creates a object of type T
         * @return Creates an object of type T
         */
        public T createObject();
    }

    private final List<T> freeObjects;
    private final PoolObjectFactory<T> factory;
    private final int maxSize;

    /**
     * This constructor initialize the factory for this object
     * and also defines the max size it can generate.
     *
     * @param factory The factory of Type T used to generate
     *                new objects for this class
     * @param maxSize The maximum number of objects this
     *                class can hold at one time.
     */
    public Pool(PoolObjectFactory<T> factory, int maxSize) {
        this.factory = factory;
        this.maxSize = maxSize;
        this.freeObjects = new ArrayList<T>(maxSize);
    }

    /**
     *  Creates a new object of type <b>T</b>
     *
     *  If the number of free objects equal 0 then create a new one.
     *  Else remove a object from the free objects
     * @return A new object of type <b>T</b>
     */
    public T newObject() {
        T object = null;

        if (freeObjects.size() == 0)
            object = factory.createObject();
        else
            object = freeObjects.remove(freeObjects.size() - 1);

        return object;
    }

    /**
     * This method adds an object to the free objects
     * only if the number of free objects does not
     * exceed the maximum size.
     * @param object The object to be added to the
     *               free objects.
     */
    public void free(T object) {
        if (freeObjects.size() < maxSize)
            freeObjects.add(object);
    }
}
