package com.kilobolt.framework;

public abstract class Screen {
    protected final Game game;

    /**
     * Constructor for the screen, The game is passed in so
     * the screen knows what to display
     * @param game The game to be shown.
     */
    public Screen(Game game) {
        this.game = game;
    }

    /**
     * This method updates the screen.
     * @param deltaTime
     */
    public abstract void update(float deltaTime);

    /**
     * This method draws a graphic onto the screen
     * @param deltaTime
     */
    public abstract void paint(float deltaTime);

    /**
     * This method pauses the screen of the game.
     */
    public abstract void pause();

    /**
     * This method resumes the current screen from its
     * paused state
     */
    public abstract void resume();

    /**
     * This method disposes of screen
     */
    public abstract void dispose();

    /**
     * This method is used to determine what action is done
     * when the back button is pressed
     */
    public abstract void backButton();
}
