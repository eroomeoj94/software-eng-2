package com.kilobolt.framework;

import com.kilobolt.framework.Graphics.ImageFormat;

public interface Image {

    /**
     * Gets the width of the image object
     *
     * @return The width of the image
     */
    public int getWidth();

    /**
     * Gets the height of the image object
     *
     * @return The height of the image
     */
    public int getHeight();

    /**
     * Gets the image format of the image object
     *
     * @return The image format of the image
     */
    public ImageFormat getFormat();

    /**
     * This method is used to dispose of the image
     * object.
     */
    public void dispose();
}
