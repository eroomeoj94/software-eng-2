package com.kilobolt.framework;

public interface Sound {

    /**
     * This method plays the Sound object at a
     * given volume
     *
     * @param volume Volume of the sound to be played.
     */
    public void play(float volume);

    /**
     * This method disposes of the Sound object.
     */
    public void dispose();
}
