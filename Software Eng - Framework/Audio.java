package com.kilobolt.framework;

public interface Audio {

    /**
     * Creates a Music object from a given file.
     *
     * @param filename The name of music file
     * @return New music object created from a file
     */
    public Music createMusic(String file);

    /**
     * Creates a Sound object from a given file
     *
     * @param filename The name of the sound file.
     * @return New sound object created asset file
     */
    public Sound createSound(String file);
}
