package com.kilobolt.framework;

import java.util.List;

public interface Input {

    /**
     * This Static class is for holding information on touch events that occur
     * in the application
     */
    public static class TouchEvent {
        public static final int TOUCH_DOWN = 0;
        public static final int TOUCH_UP = 1;
        public static final int TOUCH_DRAGGED = 2;
        public static final int TOUCH_HOLD = 3;

        public int type;
        public int x, y;
        public int pointer;


    }

    /**
     * Checks if the touch handler is touched down.
     *
     * @param pointer The position of the touch
     * @return True if it is touch down else false.
     */
    public boolean isTouchDown(int pointer);

    /**
     * Gets the x coordinate of the touch event
     * @param pointer The position of the touch
     * @return the x coordinate
     */
    public int getTouchX(int pointer);

    /**
     * Gets the y coordinate of the touch event
     * @param pointer The position of the touch
     * @return the y coordinate
     */
    public int getTouchY(int pointer);

    /**
     * This method returns a list of touch events from the touch handler.
     * @return The touch events from the touch handler
     */
    public List<TouchEvent> getTouchEvents();
}
