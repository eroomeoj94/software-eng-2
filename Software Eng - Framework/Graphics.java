package com.kilobolt.framework;

import android.graphics.Paint;

public interface Graphics {
    /**
     * Enum holding ARGB (Alpha, Red, Green and Blue) image formats.
     */
	public static enum ImageFormat {
        ARGB8888, ARGB4444, RGB565
    }

    /**
     * Creates a new image object from a given file.
     *
     * @param fileName Name of the file to be converted
     *                 to an Image
     * @param format The image format of the file.
     * @return The new image object contains the file.
     */
    public Image newImage(String fileName, ImageFormat format);

    /**
     * Clears the screen with a set color
     * @param color The color the screen will be filled with.
     */
    public void clearScreen(int color);

    /**
     * Draws a line on the canvas
     * @param x X coordinate of where the line starts.
     * @param y Y coordinate of where the line starts.
     * @param x2 X coordinate of where the line ends.
     * @param y2 Y coordinate of where the line ends.
     * @param color The color of the line.
     */
    public void drawLine(int x, int y, int x2, int y2, int color);

    /**
     * Draws a rectangle on the canvas.
     * @param x X coordinate of the where the top left of the rectangle will be.
     * @param y Y coordinate of the where the top left of the rectangle will be.
     * @param width Width of the rectangle
     * @param height Height of the rectangle
     * @param color Color of the rectangle.
     */
    public void drawRect(int x, int y, int width, int height, int color);

    /**
     * Converts a Image object and
     * draws it onto the screen within a given range
     * and with a subset of the bitmap to be drawn also.
     *
     * @param Image The images to be drawn onto the screen
     * @param x X coordinate of the origin of the image
     * @param y Y coordinate of the origin of the image
     * @param srcX The subset of the bitmap to be drawn x coordinate
     * @param srcY The subset of the bitmap to be drawn y coordinate
     * @param srcWidth The subset of the bitmap to be drawn height coordinate
     * @param srcHeight The subset of the bitmap to be drawn width coordinate
     */
    public void drawImage(Image image, int x, int y, int srcX, int srcY,
            int srcWidth, int srcHeight);

    /**
     * Converts a Image object and
     * draws it onto the screen.
     *
     * @param Image Image to be drawn onto the screen
     * @param x X coordinate of the origin of the image
     * @param y Y coordinate of the origin of the image
     */
    public void drawImage(Image Image, int x, int y);

    /**
     * Writes text onto the screen
     *
     * @param text Text to be written on the screen
     * @param x X coordinate of the origin of the text
     * @param y Y coordinate of the origin of the text
     * @param paint color of the text.
     */
    void drawString(String text, int x, int y, Paint paint);

    /**
     * Gets the width of the graphic
     * @return Width of the graphic
     */
    public int getWidth();

    /**
     * Gets the height of the graphic
     * @return Height of the graphic
     */
    public int getHeight();

    /**
     * Fills the screen with determined color (ARGB)
     *
     * @param i alpha of the color to be drawn on the canvas
     * @param j red component of the color to be drawn on the canvas
     * @param k green component of the color to be drawn on the canvas
     * @param l blue component of the color to be drawn on the canvas
     */
    public void drawARGB(int i, int j, int k, int l);
}
