package com.kilobolt.framework;

public interface Game {

    /**
     * This method is used to get the Audio object of the
     * Game.
     *
     * @return The Audio object of the game
     */
	public Audio getAudio();

    /**
     * This method is used to get the Input object of the
     * Game.
     *
     * @return The Input object of the game
     */
    public Input getInput();

    /**
     * This method is used to get the FileIO object of the
     * Game.
     *
     * @return The FileIO object of the game
     */
    public FileIO getFileIO();

    /**
     * This method is used to get the Graphics object of the
     * Game.
     *
     * @return The Graphics object of the game
     */
    public Graphics getGraphics();

    /**
     * This method is used to change the current screen of the game
     * and assign it a new screen.
     *
     * @param screen The new screen for the game
     */
    public void setScreen(Screen screen);

    /**
     * This method is for retrieving the current screen of
     * the game.
     *
     * @return The current screen of the game
     */
    public Screen getCurrentScreen();

    /**
     * This method is used to retrieve the Init Screen of the game
     *
     * @return The Init screen
     */
    public Screen getInitScreen();
	
}
