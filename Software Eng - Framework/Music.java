package com.kilobolt.framework;

public interface Music {

    /**
     * This method is used to start playing the Music
     */
    public void play();

    /**
     * This method is used stop the Music from being played.
     */
    public void stop();

    /**
     * This method is used to pause the Music.
     */
    public void pause();

    /**
     * This method is used to set the Music's looping
     * state which is determined by the parameter.
     *
     * @param looping Sets the looping to either true/false
     */
    public void setLooping(boolean looping);

    /**
     * This method is used to set the volume of the Music.
     * @param volume The level of the volume to be set.
     */
    public void setVolume(float volume);

    /**
     * This method is used to check if the Music is currently playing
     *
     * @return True if the Music is playing else false
     */
    public boolean isPlaying();

    /**
     * This method is used to check if the Music has stopped.
     * @return True if the Music has stopped else false
     */
    public boolean isStopped();

    /**
     * This method is used to check if the Music is looping
     * @return True if the Music is looping.
     */
    public boolean isLooping();

    /**
     * This method is used to dispose of the Music
     */
    public void dispose();

    /**
     * This method is used to seek the beginning of the Music
     */
    void seekBegin();
}