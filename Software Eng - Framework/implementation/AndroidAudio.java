package com.kilobolt.framework.implementation;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;

import com.kilobolt.framework.Audio;
import com.kilobolt.framework.Music;
import com.kilobolt.framework.Sound;

public class AndroidAudio implements Audio {
    AssetManager assets;
    SoundPool soundPool;

    /**
     * Creates Android Audio:
     *  Sets the volume control stream to stream music
     *  Gets the activity assets
     *  Initialise the sound pool.
     *
     * @param activity Current activity
     */
    public AndroidAudio(Activity activity) {
        activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        this.assets = activity.getAssets();
        this.soundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
    }

    /**
     * Creates a Music object from a given asset.
     *
     * @param filename The name of music file
     * @return New music object created from asset file
     * @throws IOException from if cannot load music
     */
    @Override
    public Music createMusic(String filename) {
        try {
            AssetFileDescriptor assetDescriptor = assets.openFd(filename);
            return new AndroidMusic(assetDescriptor);
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load music '" + filename + "'");
        }
    }

    /**
     * Creates a Sound object from a given asset
     *
     * @param filename The name of the sound file.
     * @return New sound object created asset file
     * @throws IOException from if cannot load sound
     */
    @Override
    public Sound createSound(String filename) {
        try {
            AssetFileDescriptor assetDescriptor = assets.openFd(filename);
            int soundId = soundPool.load(assetDescriptor, 0);
            return new AndroidSound(soundPool, soundId);
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load sound '" + filename + "'");
        }
    }
}
