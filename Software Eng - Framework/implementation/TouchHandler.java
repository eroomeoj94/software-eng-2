package com.kilobolt.framework.implementation;

import java.util.List;

import android.view.View.OnTouchListener;

import com.kilobolt.framework.Input.TouchEvent;

public interface TouchHandler extends OnTouchListener {

    /**
     * This method is to check if a touch is down on the screen.
     *
     * @param pointer The position of the touch
     * @return True if handler is touched down.
     */
    public boolean isTouchDown(int pointer);

    /**
     * This method is to return the X coordinate of the touch
     * @param pointer The position of the touch
     * @return The x coordinate of the touch
     */
    public int getTouchX(int pointer);

    /**
     * This method is to return the Y coordinate of the touch
     * @param pointer The position of the touch
     * @return The y coordinate of the touch
     */
    public int getTouchY(int pointer);

    /**
     * This method is to retrieve the a list of
     * Touch events.
     * @return A list of touch events from the handler.
     */
    public List<TouchEvent> getTouchEvents();
}
