package com.kilobolt.framework.implementation;

import java.io.IOException;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;

import com.kilobolt.framework.Music;

public class AndroidMusic implements Music, OnCompletionListener, OnSeekCompleteListener, OnPreparedListener, OnVideoSizeChangedListener {
    MediaPlayer mediaPlayer;
    boolean isPrepared = false;

    /**
     * Creation of the AndroidMusic initializes the media player and also sets
     * the data source for the media player with the AssetFileDescriptor
     *
     * @param assetDescriptor Used for reading data in the media player.
     * @throws RuntimeException if it cannot load the music
     */
    public AndroidMusic(AssetFileDescriptor assetDescriptor) {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(assetDescriptor.getFileDescriptor(),
                    assetDescriptor.getStartOffset(),
                    assetDescriptor.getLength());
            mediaPlayer.prepare();
            isPrepared = true;
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnSeekCompleteListener(this);
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnVideoSizeChangedListener(this);
            
        } catch (Exception e) {
            throw new RuntimeException("Couldn't load music");
        }
    }

    /**
     * Disposes of the mediaPlayer. If it is already playing then it will first
     * stop the player then it will dispose of it.
     */
    @Override
    public void dispose() {
    
         if (this.mediaPlayer.isPlaying()){
               this.mediaPlayer.stop();
                }
        this.mediaPlayer.release();
    }

    /**
     * Checks if the media player is looping.
     *
     * @return True if the media player is looping else false.
     */
    @Override
    public boolean isLooping() {
        return mediaPlayer.isLooping();
    }

    /**
     * Checks if the media player is playing.
     * @return True if the media player is playering else false.
     */
    @Override
    public boolean isPlaying() {
        return this.mediaPlayer.isPlaying();
    }

    /**
     * Checks to see if the media player has stopped.
     * @return True is the media player has stopped else false.
     */
    @Override
    public boolean isStopped() {
        return !isPrepared;
    }

    /**
     * This method pauses the media player only if
     * is player.
     */
    @Override
    public void pause() {
        if (this.mediaPlayer.isPlaying())
            mediaPlayer.pause();
    }


    /**
     * This method plays the media player only if it is not already playing.
     * When starting the media player if it is not prepared then it will
     * first prepare the media player and then start the media player.
     */
    @Override
    public void play() {
        if (this.mediaPlayer.isPlaying())
            return;

        try {
            synchronized (this) {
                if (!isPrepared)
                    mediaPlayer.prepare();
                mediaPlayer.start();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * If the looping mode was being set to true then
     * the MediaPlayer object shall remain in the Started state.
     *
     * @param isLooping boolean variable for if the media player should
     *                  be looping.
     */
    @Override
    public void setLooping(boolean isLooping) {
        mediaPlayer.setLooping(isLooping);
    }

    /**
     * Sets the volume of the media player.
     *
     * @param volume The value for which the volume will be set
     *               to.
     */
    @Override
    public void setVolume(float volume) {
        mediaPlayer.setVolume(volume, volume);
    }

    /**
     * This method stops the media player only if it is
     * playing.
     */
    @Override
    public void stop() {
         if (this.mediaPlayer.isPlaying() == true){
        this.mediaPlayer.stop();
        
       synchronized (this) {
           isPrepared = false;
        }}
    }

    /**
     * This method is called on the completion of a Music file.
     * It sets the isPrepared attribute to false which means
     * it has stopped
     *
     * @param player The MediaPlayer to be stopped
     */
    @Override
    public void onCompletion(MediaPlayer player) {
        synchronized (this) {
            isPrepared = false;
        }
    }

    /**
     * This method moves the media player to the beginning of its playlist.
     */
    @Override
    public void seekBegin() {
        mediaPlayer.seekTo(0);
        
    }

    /**
     * When the player is prepared it sets the prepared attribute
     * to start.
     * @param player The player that is prepared.
     */
    @Override
    public void onPrepared(MediaPlayer player) {
        // TODO Auto-generated method stub
         synchronized (this) {
               isPrepared = true;
            }
        
    }

    @Override
    public void onSeekComplete(MediaPlayer player) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void onVideoSizeChanged(MediaPlayer player, int width, int height) {
        // TODO Auto-generated method stub
        
    }
}
