package com.kilobolt.framework.implementation;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.Window;
import android.view.WindowManager;

import com.kilobolt.framework.Audio;
import com.kilobolt.framework.FileIO;
import com.kilobolt.framework.Game;
import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Input;
import com.kilobolt.framework.Screen;

public abstract class AndroidGame extends Activity implements Game {
    AndroidFastRenderView renderView;
    Graphics graphics;
    Audio audio;
    Input input;
    FileIO fileIO;
    Screen screen;
    WakeLock wakeLock;

    /**
     * Creation of the game first request the window. Then Initializes
     * the various components of the game such the RenderView, Graphics, FileIO,
     * Audio, Input and the screen. Then makes sure the device screen is powered on.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        int frameBufferWidth = isPortrait ? 480: 800;
        int frameBufferHeight = isPortrait ? 800: 480;
        Bitmap frameBuffer = Bitmap.createBitmap(frameBufferWidth,
                frameBufferHeight, Config.RGB_565);
        
        float scaleX = (float) frameBufferWidth
                / getWindowManager().getDefaultDisplay().getWidth();
        float scaleY = (float) frameBufferHeight
                / getWindowManager().getDefaultDisplay().getHeight();

        renderView = new AndroidFastRenderView(this, frameBuffer);
        graphics = new AndroidGraphics(getAssets(), frameBuffer);
        fileIO = new AndroidFileIO(this);
        audio = new AndroidAudio(this);
        input = new AndroidInput(this, renderView, scaleX, scaleY);
        screen = getInitScreen();
        setContentView(renderView);
        
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "MyGame");
    }

    /**
     * Resumes the game, power up the screen
     * and resuming the rendering of the game.
     */
    @Override
    public void onResume() {
        super.onResume();
        wakeLock.acquire();
        screen.resume();
        renderView.resume();
    }

    /**
     * Stops rendering the game and pauses the screen.
     * If the game is finished then the screen is disposed.
     */
    @Override
    public void onPause() {
        super.onPause();
        wakeLock.release();
        renderView.pause();
        screen.pause();

        if (isFinishing())
            screen.dispose();
    }

    /**
     * Returns the games Input object.
     *
     * @return The games current Input object.
     */
    @Override
    public Input getInput() {
        return input;
    }

    /**
     * Returns the games FileIO object.
     *
     * @return The games current FileIO object.
     */
    @Override
    public FileIO getFileIO() {
        return fileIO;
    }

    /**
     * Returns the games graphics.
     *
     * @return The games current graphics
     */
    @Override
    public Graphics getGraphics() {
        return graphics;
    }

    /**
     * Returns the games Audio object.
     *
     * @return The games current Audio object.
     */
    @Override
    public Audio getAudio() {
        return audio;
    }

    /**
     * Disposes of the old screen, and updates the game with
     * a new screen.
     *
     * @param screen The new screen to be rendered
     * @throws IllegalArgumentException This is thrown if a null screen passed in.
     */
    @Override
    public void setScreen(Screen screen) {
        if (screen == null)
            throw new IllegalArgumentException("Screen must not be null");

        this.screen.pause();
        this.screen.dispose();
        screen.resume();
        screen.update(0);
        this.screen = screen;
    }

    /**
     * Gets the current screen of the game
     *
     * @return The current screen of the game.
     */
    public Screen getCurrentScreen() {

        return screen;
    }
}
