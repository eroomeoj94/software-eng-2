package com.kilobolt.framework.implementation;

import java.io.IOException;
import java.io.InputStream;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;

import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Image;

public class AndroidGraphics implements Graphics {
    AssetManager assets;
    Bitmap frameBuffer;
    Canvas canvas;
    Paint paint;
    Rect srcRect = new Rect();
    Rect dstRect = new Rect();

    /**
     * Initializes the Android Graphics object.
     *
     * @param assets The AssetManager for the graphics
     * @param frameBuffer Bitmap which holds the frame buffer.
     */
    public AndroidGraphics(AssetManager assets, Bitmap frameBuffer) {
        this.assets = assets;
        this.frameBuffer = frameBuffer;
        this.canvas = new Canvas(frameBuffer);
        this.paint = new Paint();
    }

    /**
     * Creates a new image object from a given Asset which has been
     * converted into a bitmap.
     *
     * @param fileName Name of the Asset to be converted
     *                 to an Image
     * @param format Format of the bitmap to be created.
     * @return The image from Assets.
     * @throw RuntimeException if it cannot load the bitmap from Assets.
     */
    @Override
    public Image newImage(String fileName, ImageFormat format) {
        Config config = null;
        if (format == ImageFormat.RGB565)
            config = Config.RGB_565;
        else if (format == ImageFormat.ARGB4444)
            config = Config.ARGB_4444;
        else
            config = Config.ARGB_8888;

        Options options = new Options();
        options.inPreferredConfig = config;
        
        
        InputStream in = null;
        Bitmap bitmap = null;
        try {
            in = assets.open(fileName);
            bitmap = BitmapFactory.decodeStream(in, null, options);
            if (bitmap == null)
                throw new RuntimeException("Couldn't load bitmap from asset '"
                        + fileName + "'");
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load bitmap from asset '"
                    + fileName + "'");
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }

        if (bitmap.getConfig() == Config.RGB_565)
            format = ImageFormat.RGB565;
        else if (bitmap.getConfig() == Config.ARGB_4444)
            format = ImageFormat.ARGB4444;
        else
            format = ImageFormat.ARGB8888;

        return new AndroidImage(bitmap, format);
    }

    /**
     * Clears the canvas.
     *
     * @param color
     */
    @Override
    public void clearScreen(int color) {
        canvas.drawRGB((color & 0xff0000) >> 16, (color & 0xff00) >> 8, (color & 0xff));
    }

    /**
     * Draws a line on the canvas
     * @param x X coordinate of where the line starts.
     * @param y Y coordinate of where the line starts.
     * @param x2 X coordinate of where the line ends.
     * @param y2 Y coordinate of where the line ends.
     * @param color The color of the line.
     */
    @Override
    public void drawLine(int x, int y, int x2, int y2, int color) {
        paint.setColor(color);
        canvas.drawLine(x, y, x2, y2, paint);
    }

    /**
     * Draws a rectangle on the canvas.
     * @param x X coordinate of the where the top left of the rectangle will be.
     * @param y Y coordinate of the where the top left of the rectangle will be.
     * @param width Width of the rectangle
     * @param height Height of the rectangle
     * @param color Color of the rectangle.
     */
    @Override
    public void drawRect(int x, int y, int width, int height, int color) {
        paint.setColor(color);
        paint.setStyle(Style.FILL);
        canvas.drawRect(x, y, x + width - 1, y + height - 1, paint);
    }

    /**
     * Fills the canvas with determined color
     *
     * @param a alpha of the color to be drawn on the canvas
     * @param r red component of the color to be drawn on the canvas
     * @param g green component of the color to be drawn on the canvas
     * @param b blue component of the color to be drawn on the canvas
     */
    @Override
    public void drawARGB(int a, int r, int g, int b) {
        paint.setStyle(Style.FILL);
       canvas.drawARGB(a, r, g, b);
    }

    /**
     * Writes text onto the canvas
     *
     * @param text Text to be written on the canvas
     * @param x X coordinate of the origin of the text
     * @param y Y coordinate of the origin of the text
     * @param paint color of the text.
     */
    @Override
    public void drawString(String text, int x, int y, Paint paint){
        canvas.drawText(text, x, y, paint);

        
    }

    /**
     * Converts a Image object into a bitmap and
     * draws it onto the canvas within a given range
     * and a subset of the bitmap to be drawn
     *
     * @param Image The images to be drawn onto the canvas
     * @param x X coordinate of the origin of the image
     * @param y Y coordinate of the origin of the image
     * @param srcX The subset of the bitmap to be drawn x coordinate
     * @param srcY The subset of the bitmap to be drawn y coordinate
     * @param srcWidth The subset of the bitmap to be drawn height coordinate
     * @param srcHeight The subset of the bitmap to be drawn width coordinate
     */
    public void drawImage(Image Image, int x, int y, int srcX, int srcY,
            int srcWidth, int srcHeight) {
        srcRect.left = srcX;
        srcRect.top = srcY;
        srcRect.right = srcX + srcWidth;
        srcRect.bottom = srcY + srcHeight;
        
        
        dstRect.left = x;
        dstRect.top = y;
        dstRect.right = x + srcWidth;
        dstRect.bottom = y + srcHeight;

        canvas.drawBitmap(((AndroidImage) Image).bitmap, srcRect, dstRect,
                null);
    }

    /**
     * Converts a Image object into a bitmap and
     * draws it onto the canvas.
     *
     * @param Image Image to be drawn onto the canvas
     * @param x X coordinate of the origin of the image
     * @param y Y coordinate of the origin of the image
     */
    @Override
    public void drawImage(Image Image, int x, int y) {
        canvas.drawBitmap(((AndroidImage)Image).bitmap, x, y, null);
    }

    /**
     * Converts a Image object into a bitmap and
     * draws it onto the canvas within a given range
     * and a subset of the bitmap to be drawn
     *
     * @param Image The images to be drawn onto the canvas
     * @param x X coordinate of the origin of the image
     * @param y Y coordinate of the origin of the image
     * @param width Width of the image
     * @param height Height of the image
     * @param srcX The subset of the bitmap to be drawn x coordinate
     * @param srcY The subset of the bitmap to be drawn y coordinate
     * @param srcWidth The subset of the bitmap to be drawn height coordinate
     * @param srcHeight The subset of the bitmap to be drawn width coordinate
     */
    public void drawScaledImage(Image Image, int x, int y, int width, int height, int srcX, int srcY, int srcWidth, int srcHeight){
        
        
     srcRect.left = srcX;
        srcRect.top = srcY;
        srcRect.right = srcX + srcWidth;
        srcRect.bottom = srcY + srcHeight;
        
        
        dstRect.left = x;
        dstRect.top = y;
        dstRect.right = x + width;
        dstRect.bottom = y + height;
        
   
        
        canvas.drawBitmap(((AndroidImage) Image).bitmap, srcRect, dstRect, null);
        
    }

    /**
     * Gets the width of the frame buffer.
     * @return Width of the frame buffer.
     */
    @Override
    public int getWidth() {
        return frameBuffer.getWidth();
    }

    /**
     * Gets the height of the frame buffer.
     * @return Height of the frame buffer.
     */
    @Override
    public int getHeight() {
        return frameBuffer.getHeight();
    }
}
