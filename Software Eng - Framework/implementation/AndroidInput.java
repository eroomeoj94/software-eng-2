package com.kilobolt.framework.implementation;

import java.util.List;

import android.content.Context;
import android.os.Build.VERSION;
import android.view.View;

import com.kilobolt.framework.Input;

public class AndroidInput implements Input {    
    TouchHandler touchHandler;

    /**
     * Creation of the AndroidInput requires the current context and view of the application.
     * It also requires a x & y scale to determine the scale of the touch input.
     *
     * @param context The current context of the application
     * @param view The current view of the handler
     * @param scaleX The scale of the x value in the touch event
     * @param scaleY The scale of the y value in the touch event
     */
    public AndroidInput(Context context, View view, float scaleX, float scaleY) {
        if(Integer.parseInt(VERSION.SDK) < 5) 
            touchHandler = new SingleTouchHandler(view, scaleX, scaleY);
        else
            touchHandler = new MultiTouchHandler(view, scaleX, scaleY);        
    }


    /**
     * Checks if the touch handler is touched down.
     *
     * @param pointer The position of the touch
     * @return True if it is touch down else false.
     */
    @Override
    public boolean isTouchDown(int pointer) {
        return touchHandler.isTouchDown(pointer);
    }

    /**
     * Gets the x coordinate of the touch event
     * @param pointer The position of the touch
     * @return the x coordinate
     */
    @Override
    public int getTouchX(int pointer) {
        return touchHandler.getTouchX(pointer);
    }

    /**
     * Gets the y coordinate of the touch event
     * @param pointer The position of the touch
     * @return the y coordinate
     */
    @Override
    public int getTouchY(int pointer) {
        return touchHandler.getTouchY(pointer);
    }


    /**
     * This method returns a list of touch events from the touch handler.
     * @return The touch events from the touch handler
     */
    @Override
    public List<TouchEvent> getTouchEvents() {
        return touchHandler.getTouchEvents();
    }
    
}
