package com.kilobolt.framework.implementation;

import android.graphics.Bitmap;

import com.kilobolt.framework.Image;
import com.kilobolt.framework.Graphics.ImageFormat;

public class AndroidImage implements Image {
    Bitmap bitmap;
    ImageFormat format;

    /**
     * Creation of the image object, requires two pieces of data. The bitmap image and
     * the format of the image.
     *
     * @param bitmap The bitmap image for which the image is based around.
     * @param format Format of the image.
     */
    public AndroidImage(Bitmap bitmap, ImageFormat format) {
        this.bitmap = bitmap;
        this.format = format;
    }

    /**
     * Gets the width of the image
     *
     * @return The width of the bitmap image
     */
    @Override
    public int getWidth() {
        return bitmap.getWidth();
    }

    /**
     * Gets the height of the image
     *
     * @return The height of the bitmap image
     */
    @Override
    public int getHeight() {
        return bitmap.getHeight();
    }

    /**
     * Gets the format of the image
     *
     * @return The format of the image
     */
    @Override
    public ImageFormat getFormat() {
        return format;
    }

    /**
     * This method disposes of the bitmap image from the class.
     */
    @Override
    public void dispose() {
        bitmap.recycle();
    }      
}
