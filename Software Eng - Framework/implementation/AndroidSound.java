package com.kilobolt.framework.implementation;

import android.media.SoundPool;

import com.kilobolt.framework.Sound;

public class AndroidSound implements Sound {
    int soundId;
    SoundPool soundPool;

    /**
     * The creation of the AndroidSound requires the sounds ID and
     * the sound pool (The SoundPool class manages and
     * plays audio resources for applications.)
     * @param soundPool The sound pool.
     * @param soundId The ID of the sound.
     */
    public AndroidSound(SoundPool soundPool, int soundId) {
        this.soundId = soundId;
        this.soundPool = soundPool;
    }

    /**
     * Plays the sound from the sound pool at a given volume.
     *
     * @param volume Volume of the sound to be played.
     */
    @Override
    public void play(float volume) {
        soundPool.play(soundId, volume, volume, 0, 0, 1);
    }

    /**
     * This disposes a specific sound from the sound pool.
     */
    @Override
    public void dispose() {
        soundPool.unload(soundId);
    }

}
