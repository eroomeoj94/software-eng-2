package com.kilobolt.framework.implementation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Environment;
import android.preference.PreferenceManager;

import com.kilobolt.framework.FileIO;

public class AndroidFileIO implements FileIO {
    Context context;
    AssetManager assets;
    String externalStoragePath;

    /**
     * Initializing of the Android FileIO object
     *
     * @param context The applications current context.
     */
    public AndroidFileIO(Context context) {
        this.context = context;
        this.assets = context.getAssets();
        this.externalStoragePath = Environment.getExternalStorageDirectory()
                .getAbsolutePath() + File.separator;
        
 
    
    }

    /**
     * Reads assets from the AssetManager
     *
     * @param file The name of the Asset
     * @return The file is returned as an InputStream
     * @throws IOException Throws an IOException if cannot find Asset.
     */
    @Override
    public InputStream readAsset(String file) throws IOException {
        return assets.open(file);
    }

    /**
     * Inputs a file from external storage.
     *
     * @param file The name of the file to imported
     * @return The file as an input stream
     * @throws IOException Throws an IOException if cannot find file.
     */
    @Override
    public InputStream readFile(String file) throws IOException {
        return new FileInputStream(externalStoragePath + file);
    }

    /**
     * Writes a file in external storage.
     *
     * @param file The name of the file to be written.
     * @return The file is returned as a output stream
     * @throws IOException Throws IOException if it cannot find directory
     */
    @Override
    public OutputStream writeFile(String file) throws IOException {
        return new FileOutputStream(externalStoragePath + file);
    }

    /**
     * Gets the shared preferences from the manager.
     *
     * @return The shared preferences
     */
    public SharedPreferences getSharedPref() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}
