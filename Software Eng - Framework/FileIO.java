package com.kilobolt.framework;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.SharedPreferences;

public interface FileIO {

    /**
     * Reads a specific file and returns it as a InputStream
     *
     * @param file The name of the file
     * @return The file is returned as an InputStream
     * @throws IOException Throws an IOException if cannot find file.
     */
    public InputStream readFile(String file) throws IOException;

    /**
     * Writes a file with a given file name.
     *
     * @param file The name of the file to be written
     * @return The file is returned as a output stream
     * @throws IOException Throws an IOException if cannot find file.
     */
    public OutputStream writeFile(String file) throws IOException;

    /**
     * Reads assets with a given name and returns as a
     * InputStream
     *
     * @param file The name of the Asset
     * @return The file is returned as an InputStream
     * @throws IOException Throws an IOException if cannot find Asset.
     */
    public InputStream readAsset(String file) throws IOException;

    /**
     * Gets the shared preferences
     *
     * @return The shared preferences
     */
    public SharedPreferences getSharedPref();
}
